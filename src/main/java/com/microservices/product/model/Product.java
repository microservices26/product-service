package com.microservices.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Product {
    private String id;
    @JsonProperty("model_number")
    private String modelNumber;
    private String name;
    @JsonProperty("Meta_data")
    private Object metadata;
    @JsonProperty("product_type")
    private String productType;
    @JsonProperty("Product_description")
    private Object productDescription;
    private boolean recommendationsEnabled;
    @JsonProperty("Pricing_information")
    private Object pricingInformation;
    @JsonProperty("Attribute_list")
    private Object attributeList;
    @JsonProperty("product_link_list")
    private ArrayList<Object> productLinkList = new ArrayList<>();
    @JsonProperty("view_list")
    private ArrayList<Object> viewList = new ArrayList<>();
    @JsonProperty("breadcrumb_list")
    private ArrayList<Object> breadcrumbList = new ArrayList<>();
    @JsonProperty("CalloutsObject")
    private Object calloutsObject;
    @JsonProperty("EmbellishmentObject")
    private Object embellishmentObject;

    public Product() {
    }

    public ProductWithReview toProductWithReview(ProductReview review) {
        return new ProductWithReview(id, modelNumber, name, metadata, productType, productDescription, recommendationsEnabled,
                pricingInformation, attributeList, productLinkList, viewList, breadcrumbList, calloutsObject, embellishmentObject,
                review.getAverageReviewScore(), review.getNumberOfReviews());
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setProductDescription(Object productDescription) {
        this.productDescription = productDescription;
    }

    public void setRecommendationsEnabled(boolean recommendationsEnabled) {
        this.recommendationsEnabled = recommendationsEnabled;
    }

    public void setPricingInformation(Object pricingInformation) {
        this.pricingInformation = pricingInformation;
    }

    public void setAttributeList(Object attributeList) {
        this.attributeList = attributeList;
    }

    public void setProductLinkList(ArrayList<Object> productLinkList) {
        this.productLinkList = productLinkList;
    }

    public void setViewList(ArrayList<Object> viewList) {
        this.viewList = viewList;
    }

    public void setBreadcrumbList(ArrayList<Object> breadcrumbList) {
        this.breadcrumbList = breadcrumbList;
    }

    public void setCalloutsObject(Object calloutsObject) {
        this.calloutsObject = calloutsObject;
    }

    public void setEmbellishmentObject(Object embellishmentObject) {
        this.embellishmentObject = embellishmentObject;
    }
}
