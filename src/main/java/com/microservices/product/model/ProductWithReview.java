package com.microservices.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ProductWithReview {
    private String id;
    @JsonProperty("model_number")
    private String modelNumber;
    private String name;
    @JsonProperty("Meta_data")
    private Object metadata;
    @JsonProperty("product_type")
    private String productType;
    @JsonProperty("Product_description")
    private Object productDescription;
    private boolean recommendationsEnabled;
    @JsonProperty("Pricing_information")
    private Object pricingInformation;
    @JsonProperty("Attribute_list")
    private Object attributeList;
    @JsonProperty("product_link_list")
    private ArrayList<Object> productLinkList = new ArrayList<>();
    @JsonProperty("view_list")
    private ArrayList<Object> viewList = new ArrayList<>();
    @JsonProperty("breadcrumb_list")
    private ArrayList<Object> breadcrumbList = new ArrayList<>();
    @JsonProperty("CalloutsObject")
    private Object calloutsObject;
    @JsonProperty("EmbellishmentObject")
    private Object embellishmentObject;

    private Double averageReviewScore;
    private Integer numberOfReviews;

    public ProductWithReview() {
    }

    public ProductWithReview(String id, String modelNumber, String name, Object metadata, String productType, Object productDescription,
                             boolean recommendationsEnabled, Object pricingInformation, Object attributeList, ArrayList<Object> productLinkList,
                             ArrayList<Object> viewList, ArrayList<Object> breadcrumbList, Object calloutsObject, Object embellishmentObject,
                             Double averageReviewScore, Integer numberOfReviews) {
        this.id = id;
        this.modelNumber = modelNumber;
        this.name = name;
        this.metadata = metadata;
        this.productType = productType;
        this.productDescription = productDescription;
        this.recommendationsEnabled = recommendationsEnabled;
        this.pricingInformation = pricingInformation;
        this.attributeList = attributeList;
        this.productLinkList = productLinkList;
        this.viewList = viewList;
        this.breadcrumbList = breadcrumbList;
        this.calloutsObject = calloutsObject;
        this.embellishmentObject = embellishmentObject;
        this.averageReviewScore = averageReviewScore;
        this.numberOfReviews = numberOfReviews;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getMetadata() {
        return metadata;
    }

    public void setMetadata(Object metadata) {
        this.metadata = metadata;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Object getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(Object productDescription) {
        this.productDescription = productDescription;
    }

    public boolean isRecommendationsEnabled() {
        return recommendationsEnabled;
    }

    public void setRecommendationsEnabled(boolean recommendationsEnabled) {
        this.recommendationsEnabled = recommendationsEnabled;
    }

    public Object getPricingInformation() {
        return pricingInformation;
    }

    public void setPricingInformation(Object pricingInformation) {
        this.pricingInformation = pricingInformation;
    }

    public Object getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(Object attributeList) {
        this.attributeList = attributeList;
    }

    public ArrayList<Object> getProductLinkList() {
        return productLinkList;
    }

    public void setProductLinkList(ArrayList<Object> productLinkList) {
        this.productLinkList = productLinkList;
    }

    public ArrayList<Object> getViewList() {
        return viewList;
    }

    public void setViewList(ArrayList<Object> viewList) {
        this.viewList = viewList;
    }

    public ArrayList<Object> getBreadcrumbList() {
        return breadcrumbList;
    }

    public void setBreadcrumbList(ArrayList<Object> breadcrumbList) {
        this.breadcrumbList = breadcrumbList;
    }

    public Object getCalloutsObject() {
        return calloutsObject;
    }

    public void setCalloutsObject(Object calloutsObject) {
        this.calloutsObject = calloutsObject;
    }

    public Object getEmbellishmentObject() {
        return embellishmentObject;
    }

    public void setEmbellishmentObject(Object embellishmentObject) {
        this.embellishmentObject = embellishmentObject;
    }

    public Double getAverageReviewScore() {
        return averageReviewScore;
    }

    public void setAverageReviewScore(Double averageReviewScore) {
        this.averageReviewScore = averageReviewScore;
    }

    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }
}
