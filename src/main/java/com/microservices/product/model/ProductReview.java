package com.microservices.product.model;

public class ProductReview {
    private String productId;
    private Double averageReviewScore;
    private Integer numberOfReviews;

    public ProductReview(){}

    public ProductReview(String productId, Double averageReviewScore, Integer numberOfReviews) {
        this.productId = productId;
        this.averageReviewScore = averageReviewScore;
        this.numberOfReviews = numberOfReviews;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getAverageReviewScore() {
        return averageReviewScore;
    }

    public void setAverageReviewScore(Double averageReviewScore) {
        this.averageReviewScore = averageReviewScore;
    }

    public Integer getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(Integer numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }
}
