package com.microservices.product.exception;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Map;

public class ProductNotFoundException extends ApiException {
    public ProductNotFoundException(String id) {
        super(new ApiError(LocalDateTime.now(), HttpStatus.NOT_FOUND, "Product Not Found (id: " + id + ")", Map.of()),
                "Product Review Not Found (id: " + id + ")");
    }
}
