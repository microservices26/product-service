package com.microservices.product.exception;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Map;

public class ClientCallException extends ApiException {
    public ClientCallException(String id, Throwable e) {
        super(new ApiError(LocalDateTime.now(), HttpStatus.FAILED_DEPENDENCY, String.format("Error while calling external dependency for product: %s", id), Map.of()),
                "Calling external dependency (exception: " + e.toString() + ")");
    }
}
