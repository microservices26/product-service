package com.microservices.product.controller;

import com.microservices.product.model.ProductWithReview;
import com.microservices.product.service.ProductReviewService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;


@RestController()
@RequestMapping("/product")
@CrossOrigin
public class ProductController {
    private final ProductReviewService productReviewService;

    public ProductController(ProductReviewService productReviewService) {
        this.productReviewService = productReviewService;
    }

    @GetMapping("/{id}")
    public Mono<ProductWithReview> getProductWithReview(@PathVariable String id) {
        return productReviewService.getProductWithReview(id);
    }
}
