package com.microservices.product.service;

import com.microservices.product.exception.ClientCallException;
import com.microservices.product.exception.ProductNotFoundException;
import com.microservices.product.model.Product;
import com.microservices.product.model.ProductReview;
import com.microservices.product.model.ProductWithReview;
import com.microservices.product.service.client.ProductClient;
import com.microservices.product.service.client.ReviewClient;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class ProductReviewService {
    private final ProductClient productClient;
    private final ReviewClient reviewClient;

    public ProductReviewService(ProductClient productClient, ReviewClient reviewClient) {
        this.productClient = productClient;
        this.reviewClient = reviewClient;
    }

    public Mono<ProductWithReview> getProductWithReview(String id) {
        return Mono.zip(getProduct(id), getReview(id))
                .map((productReviewPair) ->
                        productReviewPair.getT1().toProductWithReview(productReviewPair.getT2())
                );
    }

    private Mono<Product> getProduct(String id) {
        return productClient.getProduct(id)
                .switchIfEmpty(Mono.error(new ProductNotFoundException(id)))
                .onErrorMap(e -> {
                    if (e.getMessage().contains("404 Not Found")) {
                        return new ProductNotFoundException(id);
                    } else {
                        return new ClientCallException(id, e);
                    }
                });
    }

    private Mono<ProductReview> getReview(String id) {
        return reviewClient.getProductReview(id)
                .onErrorResume(e -> {
                    if (e.getMessage().contains("404 Not Found")) {
                        // if review doesn't exist return the product with no reviews
                        return Mono.just(new ProductReview(id, 0.0, 0));
                    } else {
                        return Mono.error(new ClientCallException(id, e));
                    }
                });
    }
}
