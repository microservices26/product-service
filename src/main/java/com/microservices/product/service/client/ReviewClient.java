package com.microservices.product.service.client;

import com.microservices.product.model.ProductReview;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class ReviewClient {
    @Value("${hosts.product-review-service-host}")
    private String productReviewServiceHost;

    private WebClient webClient = WebClient.create();

    public Mono<ProductReview> getProductReview(String id) {
        return webClient.get()
                .uri(String.format("%s/review/%s", productReviewServiceHost, id))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ProductReview.class);
    }
}
