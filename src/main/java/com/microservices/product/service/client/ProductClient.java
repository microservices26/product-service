package com.microservices.product.service.client;

import com.microservices.product.model.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class ProductClient {
    @Value("${hosts.product-service-host}")
    private String productServiceHost;

    private WebClient webClient = WebClient.create();

    public Mono<Product> getProduct(String id) {
        return webClient.get()
                .uri(String.format("%s/api/products/%s", productServiceHost, id))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Product.class);
    }
}
