package com.microservices.product;

public class Resources {
    public static String apiProductExampleResult = "{\n" +
            "\"id\":\"BB5476\",\n" +
            "\"model_number\":\"IAZ12\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"meta_data\":{\n" +
            "\"canonical\":\"//www.adidas.co.uk/gazelle-shoes/BB5476.html\",\n" +
            "\"description\":\"Shop for Gazelle Shoes - Black at adidas.co.uk! See all the styles and colours of Gazelle Shoes - " +
            "Black at the official adidas UK online store.\",\n" +
            "\"keywords\":\"Gazelle Shoes\",\n" +
            "\"page_title\":\"adidas Gazelle Shoes - Black | adidas UK\",\n" +
            "\"site_name\":\"adidas United Kingdom\"\n" +
            "},\n" +
            "\"product_type\":\"inline\",\n" +
            "\"product_description\":{\n" +
            "\"title\":\"Gazelle Shoes\",\n" +
            "\"subtitle\":\"The 1991 Gazelle returns in a one-to-one reissue.\",\n" +
            "\"text\":\"A low-profile classic. The Gazelle shoe started life as a football trainer and grew into an iconic streetwear staple. This pair honours the favorite version of 1991, " +
            "with the same materials, colours and slightly wider proportions. A leather upper gives these shoes a smooth touch and soft feel.\",\n" +
            "\"description_assets\":{\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/de4fe345315746039934a7fb0080deaa_9366/Gazelle_Shoes_Black_BB5476.jpg\",\n" +
            "\"video_url\":null,\n" +
            "\"poster_url\":null\n" +
            "},\n" +
            "\"usps\":[\n" +
            "\"Lace closure\",\n" +
            "\"Leather upper\",\n" +
            "\"Rubber outsole\",\n" +
            "\"Enjoy the comfort and performance of OrthoLite® sockliner\"\n" +
            "],\n" +
            "\"wash_care_instructions\":{\n" +
            "\"care_instructions\":[\n" +
            "]\n" +
            "}\n" +
            "},\n" +
            "\"recommendationsEnabled\":true,\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70,\n" +
            "\"standard_price_no_vat\":58.33,\n" +
            "\"currentPrice\":70\n" +
            "},\n" +
            "\"attribute_list\":{\n" +
            "\"collection\":[\n" +
            "\"Paris\",\n" +
            "\"Les Favoris des Français\",\n" +
            "\"London\",\n" +
            "\"Vintage\",\n" +
            "\"Mini Me\",\n" +
            "\"Gifts\",\n" +
            "\"BS\",\n" +
            "\"Best sellers\",\n" +
            "\"test_plp_1\",\n" +
            "\"test_plp_2\",\n" +
            "\"test_plp_3\",\n" +
            "\"test_plp_4\",\n" +
            "\"test_plp_5\",\n" +
            "\"test_plp_6\",\n" +
            "\"test_plp_7\",\n" +
            "\"test_plp_8\",\n" +
            "\"HOME OF CLASSICS\"\n" +
            "],\n" +
            "\"productType\":[\n" +
            "\"Trainers\",\n" +
            "\"Lifestyle Trainers\"\n" +
            "],\n" +
            "\"sport\":[\n" +
            "\"Lifestyle\"\n" +
            "],\n" +
            "\"sportSub\":[\n" +
            "\"Gazelle\",\n" +
            "\"Trefoil\",\n" +
            "\"Classics\"\n" +
            "],\n" +
            "\"isInPreview\":false,\n" +
            "\"customizable\":false,\n" +
            "\"isCnCRestricted\":false,\n" +
            "\"isWaitingRoomProduct\":false,\n" +
            "\"mandatory_personalization\":false,\n" +
            "\"outlet\":false,\n" +
            "\"personalizable\":true,\n" +
            "\"sale\":false,\n" +
            "\"specialLaunch\":false,\n" +
            "\"special_launch_type\":\"NONE\",\n" +
            "\"gender\":\"U\",\n" +
            "\"category\":\"Shoes\",\n" +
            "\"search_color\":\"Black\",\n" +
            "\"search_color_raw\":\"Black\",\n" +
            "\"brand\":\"Originals\",\n" +
            "\"color\":\"Core Black / Footwear White / Clear Granite\",\n" +
            "\"pricebook\":\"adidas-GB-listprices\",\n" +
            "\"coming_soon_signup\":false,\n" +
            "\"preview_to\":\"2012-12-11T22:00:00.000Z\",\n" +
            "\"size_chart_link\":\"/on/demandware.store/Sites-adidas-GB-Site/en_GB/Page-Include?cid=size-chart-size-shoes\"\n" +
            "},\n" +
            "\"product_link_list\":[\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/B41645.html\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Collegiate Burgundy / Cloud White / Cloud White\",\n" +
            "\"search_color\":\"Burgundy\",\n" +
            "\"productId\":\"B41645\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/340aeb9ac43847fea000a8da0182b561_9366/Gazelle_Shoes_Burgundy_B41645_01_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/BB5472.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/28e054c800ea44a4a8bea7fb007fc05d_9366/Gazelle_Shoes_Pink_BB5472_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw12931899/zoom/BB5472_02_hover_frv.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Vapor Pink / White / Gold Metallic\",\n" +
            "\"search_color\":\"Pink\",\n" +
            "\"productId\":\"BB5472\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/BB5478.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/698e41ae0196408eb16aa7fb008046ad_9366/Gazelle_Shoes_Blue_BB5478_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwe90af2a0/zoom/BB5478_02_hover_frv.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Collegiate Navy / White / Gold Metallic\",\n" +
            "\"search_color\":\"Blue\",\n" +
            "\"productId\":\"BB5478\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/BB5480.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/a847e062376346299a7ca80200807c9c_9366/Gazelle_Shoes_Grey_BB5480_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dwe9d2ab75/zoom/BB5480_02_hover_frv.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Dark Grey Heather / White / Gold Metallic\",\n" +
            "\"search_color\":\"Grey\",\n" +
            "\"productId\":\"BB5480\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/BB5498.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/4c9364eb1521438a8b8ba627009a6a1f_9366/Gazelle_Shoes_White_BB5498_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw9e450b7e/zoom/BB5498_02_standard.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Cloud White / Cloud White / Gold Metallic\",\n" +
            "\"search_color\":\"White\",\n" +
            "\"productId\":\"BB5498\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/BD7479.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/3a25eabed6504959b6aca98300b43436_9366/Gazelle_Shoes_White_BD7479_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw44c466de/zoom/BD7479_010_hover_standard.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":74.95\n" +
            "},\n" +
            "\"default_color\":\"Cloud White / Cloud White / Gum4\",\n" +
            "\"search_color\":\"White\",\n" +
            "\"productId\":\"BD7479\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/BD7480.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/4200f7da9a394801b578a983012049f2_9366/Gazelle_Shoes_Black_BD7480_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw496f66a6/zoom/BD7480_010_hover_standard.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":74.95\n" +
            "},\n" +
            "\"default_color\":\"Core Black / Core Black / Gum 3\",\n" +
            "\"search_color\":\"Black\",\n" +
            "\"productId\":\"BD7480\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/CQ2809.html\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Core Black / Core Black / Core Black\",\n" +
            "\"search_color\":\"Black\",\n" +
            "\"productId\":\"CQ2809\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/97f86eede1374615a058a81700a27444_9366/Gazelle_Shoes_Black_CQ2809_01_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/EE8943.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/b26c58379b934a52ac97a9d80110e9cc_9366/Gazelle_Shoes_Grey_EE8943_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw2dd35397/zoom/EE8943_010_hover_standard.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Grey Four / Core Black / Gum5\",\n" +
            "\"search_color\":\"Grey\",\n" +
            "\"productId\":\"EE8943\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/EE8947.html\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/2c839f6fd06c4c8fb1a2a9d7010922e5_9366/Gazelle_Shoes_Green_EE8947_01_standard.jpg\",\n" +
            "\"altImage\":\"//www.adidas.co.uk/dw/image/v2/aagl_prd/on/demandware.static/-/Sites-adidas-products/default/dw6e25aa3c/zoom/EE8947_010_hover_standard.jpg\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Night Cargo / Core Black / Gum5\",\n" +
            "\"search_color\":\"Green\",\n" +
            "\"productId\":\"EE8947\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/FX5480.html\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"default_color\":\"Hazy Blue / Core Black / Cloud White\",\n" +
            "\"search_color\":\"Blue\",\n" +
            "\"productId\":\"FX5480\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/da699b875d0440bcab9dac6300b3e033_9366/Gazelle_Shoes_Blue_FX5480_01_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Gazelle Shoes\",\n" +
            "\"url\":\"/gazelle-shoes/FX5485.html\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":70\n" +
            "},\n" +
            "\"badge_text\":\"New\",\n" +
            "\"badge_style\":\"new\",\n" +
            "\"default_color\":\"Core Black / Fox Orange / Cloud White\",\n" +
            "\"search_color\":\"Black\",\n" +
            "\"productId\":\"FX5485\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/00e42b5cd92941c59616ac8a011ec40a_9366/Gazelle_Shoes_Black_FX5485_01_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"color-variation\",\n" +
            "\"name\":\"Star Wars Mandalorian Gazelle Darksaber Shoes\",\n" +
            "\"url\":\"/star-wars-mandalorian-gazelle-darksaber-shoes/GZ2753.html\",\n" +
            "\"pricing_information\":{\n" +
            "\"standard_price\":74.95\n" +
            "},\n" +
            "\"badge_text\":\"Sold out\",\n" +
            "\"badge_style\":\"soldout\",\n" +
            "\"default_color\":\"Core Black / Silver Metallic / Grey Four\",\n" +
            "\"search_color\":\"Black\",\n" +
            "\"productId\":\"GZ2753\",\n" +
            "\"image\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/2eaa88a08b5e403abe5cabed00b457af_9366/Star_Wars_Mandalorian_Gazelle_Darksaber_Shoes_Black_GZ2753_01_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "}\n" +
            "],\n" +
            "\"view_list\":[\n" +
            "{\n" +
            "\"type\":\"standard\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/61f87dec481e4512823ea7fb0080ba1a_9366/Gazelle_Shoes_Black_BB5476_01_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"other\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/b94b0e16ce1343f8bc3ca60501799631_9366/Gazelle_Shoes_Black_BB5476_02_standard_hover.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"standard\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/8eaa5605d5294d348fd8a60501798ba0_9366/Gazelle_Shoes_Black_BB5476_03_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"standard\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/de4fe345315746039934a7fb0080deaa_9366/Gazelle_Shoes_Black_BB5476_04_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"standard\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/4bea833201784323a15ca7fb0080eb80_9366/Gazelle_Shoes_Black_BB5476_05_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"standard\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/68b2cce6c1db4120a89ea7fb0080c53d_9366/Gazelle_Shoes_Black_BB5476_06_standard.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"detail\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/e940a084cde441a48cb1a6050179b4c0_9366/Gazelle_Shoes_Black_BB5476_41_detail.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"detail\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/2bda44fe705e4af69cf7a6050179bb6a_9366/Gazelle_Shoes_Black_BB5476_42_detail.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "},\n" +
            "{\n" +
            "\"type\":\"detail\",\n" +
            "\"image_url\":\"https://assets.adidas.com/images/w_600,f_auto,q_auto/66523e8b142f415d986da7fb008104c8_9366/Gazelle_Shoes_Black_BB5476_43_detail.jpg\",\n" +
            "\"source\":\"CLOUDINARY\"\n" +
            "}\n" +
            "],\n" +
            "\"breadcrumb_list\":[\n" +
            "{\n" +
            "\"text\":\"Originals\",\n" +
            "\"link\":\"/originals\"\n" +
            "},\n" +
            "{\n" +
            "\"text\":\"Shoes\",\n" +
            "\"link\":\"/originals-shoes\"\n" +
            "}\n" +
            "],\n" +
            "\"callouts\":{\n" +
            "\"callout_bottom_stack\":[\n" +
            "{\n" +
            "\"id\":\"pdp-callout-bottom\",\n" +
            "\"iconID\":\"usp-delivery\",\n" +
            "\"title\":\"Free delivery for Creators Club members\",\n" +
            "\"body\":\"<p>Creators Club members temporarily unlock free delivery on any order. <a href=\\\"/account-register\\\">Sign up</a> or <a href=\\\"/account-login\\\">Login</a>." +
            "</p>\\r\\n\\r\\n<p>For non-members, spend over £25 and delivery is free!</p>\",\n" +
            "\"link_text\":\"Free delivery for Creators Club members\"\n" +
            "},\n" +
            "{\n" +
            "\"id\":\"pdp-callout-bottom-2\",\n" +
            "\"iconID\":\"exchange\",\n" +
            "\"title\":\"60 days free returns & exchange\",\n" +
            "\"body\":\"<p>Tried on your item(s) and need a different size? Exchange your item(s) within 60 days and have your perfect fit shipped for free.</p>\\r\\n\\r\\n<p>" +
            "Or did you change your mind? You can also return your item(s) for free within 60 days for a full refund. Customised products are made just for you and therefore cannot be returned." +
            "</p>\\r\\n\\r\\n<p>Hype products<br />\\r\\nPlease note - some products with limited availability such as YEEZY can be returned for 14 days.</p>\\r\\n\\r\\n<p>Read more on " +
            "<a href=\\\"/help/can-i-exchange-my-products.html\\\">Exchange</a><br />\\r\\nRead more on <a href=\\\"/help/how-do-i-return-my-products.html\\\">Return</a></p>\",\n" +
            "\"link_text\":\"60 days free returns & exchange\"\n" +
            "},\n" +
            "{\n" +
            "\"id\":\"pdp-callout-bottom-3\",\n" +
            "\"title\":\"Buy now, pay in 3 instalments\",\n" +
            "\"body\":\"<p><br />\\r\\nTry before you pay in full - pay in 3 easy instalments with Klarna.<br />\\r\\n<br />\\r\\n<ins><a href=" +
            "\\\"https://www.adidas.co.uk/help/pay-in--instalments-with-klarna--how-does-it-work.html\\\">LEARN MORE</a></ins></p>\",\n" +
            "\"link_text\":\"Buy now, pay in 3 instalments\"\n" +
            "}\n" +
            "]\n" +
            "},\n" +
            "\"embellishment\":{\n" +
            "\"embellishmentOptions\":[\n" +
            "{\n" +
            "\"position\":\"leftShoe\",\n" +
            "\"positionPrice\":5,\n" +
            "\"allowChooseOwnText\":true,\n" +
            "\"fields\":[\n" +
            "{\n" +
            "\"type\":\"text\",\n" +
            "\"key\":\"leftShoeText\",\n" +
            "\"maxLength\":11,\n" +
            "\"minLength\":0,\n" +
            "\"validation\":\"^[#@?&%$0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ ]*$\",\n" +
            "\"textColor\":\"adidas - 401 White\",\n" +
            "\"usesStock\":false,\n" +
            "\"stockCollection\":null\n" +
            "}\n" +
            "]\n" +
            "},\n" +
            "{\n" +
            "\"position\":\"rightShoe\",\n" +
            "\"positionPrice\":5,\n" +
            "\"allowChooseOwnText\":true,\n" +
            "\"fields\":[\n" +
            "{\n" +
            "\"type\":\"text\",\n" +
            "\"key\":\"rightShoeText\",\n" +
            "\"maxLength\":11,\n" +
            "\"minLength\":0,\n" +
            "\"validation\":\"^[#@?&%$0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ ]*$\",\n" +
            "\"textColor\":\"adidas - 401 White\",\n" +
            "\"usesStock\":false,\n" +
            "\"stockCollection\":null\n" +
            "}\n" +
            "]\n" +
            "}\n" +
            "],\n" +
            "\"articleType\":\"footwear\"\n" +
            "}\n" +
            "}";

}
