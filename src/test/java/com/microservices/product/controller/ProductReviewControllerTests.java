package com.microservices.product.controller;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.microservices.product.exception.ApiError;
import com.microservices.product.model.ProductWithReview;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.microservices.product.Resources.apiProductExampleResult;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureWireMock(port = 7777)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ProductReviewControllerTests {
    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void notFoundWhenGetNonExistingProductNorReview() {
        String productId = "NON_EXISTING";
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/products/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(404)));
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/review/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(404)));

        webTestClient.get()
                .uri(String.format("/product/%s", productId))
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void notFoundWhenGetNonExistingProductEvenIfReviewExists() {
        String productId = "NON_EXISTING";
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/products/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(404)));
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/review/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(String.format("{ \"productId\": \"%s\", \"averageReviewScore\": 1.0, \"numberOfReviews\": 1}", productId))));

        webTestClient.get()
                .uri(String.format("/product/%s", productId))
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    public void dependencyErrorWhenProductAndReviewServicesFail() {
        String productId = "ANY";
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/products/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(500)));
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/review/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(500)));

        ApiError response = webTestClient.get()
                .uri(String.format("/product/%s", productId))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.FAILED_DEPENDENCY)
                .expectBody(ApiError.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.FAILED_DEPENDENCY);
        assertThat(response.getError()).isEqualTo("Error while calling external dependency for product: ANY");
    }

    @Test
    public void dependencyErrorWhenProductFail() {
        String productId = "BB5476";
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/products/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(500)));
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/review/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(String.format("{ \"productId\": \"%s\", \"averageReviewScore\": 1.0, \"numberOfReviews\": 1}", productId))));

        ApiError response = webTestClient.get()
                .uri(String.format("/product/%s", productId))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.FAILED_DEPENDENCY)
                .expectBody(ApiError.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.FAILED_DEPENDENCY);
        assertThat(response.getError()).isEqualTo("Error while calling external dependency for product: BB5476");
    }

    @Test
    public void dependencyErrorWhenProductReviewFail() {
        String productId = "BB5476";
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/products/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apiProductExampleResult)));
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/review/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(500).withBody("{}")));

        ApiError response = webTestClient.get()
                .uri(String.format("/product/%s", productId))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.FAILED_DEPENDENCY)
                .expectBody(ApiError.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(HttpStatus.FAILED_DEPENDENCY);
        assertThat(response.getError()).isEqualTo("Error while calling external dependency for product: BB5476");
    }

    @Test
    public void getProductReviewWithNoReviews() {
        String productId = "BB5476";
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/products/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apiProductExampleResult)));
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/review/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(404)));

        ProductWithReview response = webTestClient.get()
                .uri(String.format("/product/%s", productId))
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductWithReview.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getId()).isEqualTo("BB5476");
        assertThat(response.getModelNumber()).isEqualTo("IAZ12");
        assertThat(response.getName()).isEqualTo("Gazelle Shoes");

        assertThat(response.getAverageReviewScore()).isEqualTo(0.0);
        assertThat(response.getNumberOfReviews()).isEqualTo(0);
    }

    @Test
    public void getProductReviewWithReviews() {
        String productId = "BB5476";
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/api/products/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(apiProductExampleResult)));
        WireMock.stubFor(WireMock.get(urlEqualTo(String.format("/review/%s", productId)))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(String.format("{ \"productId\": \"%s\", \"averageReviewScore\": 1.0, \"numberOfReviews\": 1}", productId))));


        ProductWithReview response = webTestClient.get()
                .uri(String.format("/product/%s", productId))
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProductWithReview.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response.getId()).isEqualTo("BB5476");
        assertThat(response.getModelNumber()).isEqualTo("IAZ12");
        assertThat(response.getName()).isEqualTo("Gazelle Shoes");

        assertThat(response.getAverageReviewScore()).isEqualTo(1.0);
        assertThat(response.getNumberOfReviews()).isEqualTo(1);
    }
}
